# krakeenDB - the hybrid data repository for omics data.
To learn more about krakenDB please read the poster.pdf and short_poster.pdf in the documents folder. 

Details documentation coming soon. 

## What it does
krakenDB was initially written to save time in merges between multi omics data. It can query existing tabular data structures and/or convert them into a single structure as dictated by the user. 

## Workflow
Before queries can be made, krakenDB needs to be fed with data and metadata. Metadata can include gene related metadata such as gene synonyms , and pathway information and patient metadata. 


krakenDB can read / process almost any tabular data including csv, tsv, relational databases and hdf5. 


