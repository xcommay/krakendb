"""

Converts imformation in a metadata file into a network

"""

import networkx as nx
import os
import cPickle
#test to see if pickle exists
network_pickle_loc = "/mnt/ceph_market/Shared/myCode/krakendb/krakenDB/utils/g.pickle"

if os.path.isfile(network_pickle_loc):
    #file exists
    g = cPickle.load(open(network_pickle_loc, "rb"))

else:
    g = nx.DiGraph()


import sys
import pandas
import glob
# glob.glob("../../../test_data/metadata/*.csv")[0].split('/')[-1]
files =  glob.glob(sys.argv[1].strip())



for f in files:
    print "working on" , f



    df = pandas.read_csv(f)
    file_name = f.split('/')[-1].split('.')[0]
    #print "working on ", file_name , f

    row_header_name = df.columns[0]
    df.index = df[row_header_name]
    del df[row_header_name]
    g.add_edge("meta_files", file_name)
    for c in df.columns:

        #print "working on", c
        g.add_edge(file_name, c)
        data = df[c].dropna()
        ix = data.index
        ix_values = data.values
        length = len(ix)
        g.add_edges_from(zip([c] * length,ix_values))
        g.add_edges_from(zip(ix_values, ix))
        g.add_edges_from(zip(["ID"] * length, ix))
    g.add_edges_from(zip([file_name]* length , ix))


#saving network as pickle
cPickle.dump(g, open(network_pickle_loc, "wb"), protocol=cPickle.HIGHEST_PROTOCOL)
