import blaze
import pandas
import networkx
import cPickle

class krakenDB():

    def __init__(self,network_name=None):
        if network_name == None:
            #create a new network
            import networkx as nx
            self.network = nx.DiGraph()
        else:
	    #there is a network
            if ".pickle" in netwrok_name:
                #the network is pickled
                import cPickle
                network = cPickle.load(open(network_name), "rb")
                self.network = network


    def network_update_network(self,new_network_object):
        self.network = new_network_object

    def network_traverse_recur(self,start_node,network=None):
        if network == None:
            network = self.network
        return networkx.ancestors(network,start_node)


    def network_traverse(self, start_node,node_filter=(), edge_filter=(),network=""):
        """
        Does a one step traversal based on the given node_filter and edge_filter.

        Args:
         start_node: name of the start node
         node_filter: tuple (attribute,operation,value)
         edge_filter: tuple (attribute,operation,value)
         attribute: name of attribute
         operation: ==, <, <=, >,>=, in
        """
        output = []
        if network == "":
            network = self.network
        #gee et all the downstream nodes.
        node_dict = network[start_node]
        if edge_filter == () and node_filter == ():
            #a traversing without any filter
            return node_dict.keys()
        if edge_filter !=  ():
            #starting edge filtering
            attribute,operation,value = edge_filter
            #safety check
            approved_attributes = ['==', '<', '<=', '>','>=', 'in']
            if operation not in approved_attributes:
                return "ERROR: unsafe attribute in edge_filter"
            #safe
            for i in node_dict:
                if operation == "in":
                    if value in node_dict[i][attribute]:
                        output.append(i)
                elif operation == "not in":
                    if value not in node_dict[i][attribute]:
                        output.append(i)
                elif operation == "==":
                    if node_dict[i][attribute] == value:
                        output.append(i)
                elif operation == ">=":
                    if node_dict[i][attribute] >= value:
                        output.append(i)
                elif operation == "<=":
                    if node_dict[i][attribute] <= value:
                        output.append(i)
                elif operation == ">":
                    if node_dict[i][attribute] > value:
                        output.append(i)
                elif operation == "<":
                    if node_dict[i][attribute] < value:
                        output.append(i)
                elif operation == "!=":
                    if node_dict[i][attribute] != value:
                        output.append(i)
        else:
            output = node_dict.keys()

        if node_filter != ():
            #starting node filtering
            attribute,operation,value = node_filter
            #safety check
            approved_attributes = ['==', '<', '<=', '>','>=', 'in']
            if operation not in approved_attributes:
                return "ERROR: unsafe attribute in node_filter"
            output2 = output
            output = []
            for i in output2:
                node_dict = network.node[i]
                if operation == "in":
                    if value in node_dict[attribute]:
                        output.append(i)
                elif operation == "not in":
                    if value not in node_dict[attribute]:
                        output.append(i)
                elif operation == "==":
                    if node_dict[attribute] == value:
                        output.append(i)
                elif operation == ">=":
                    if node_dict[attribute] >= value:
                        output.append(i)
                elif operation == "<=":
                    if node_dict[attribute] <= value:
                        output.append(i)
                elif operation == ">":
                    if node_dict[attribute] > value:
                        output.append(i)
                elif operation == "<":
                    if node_dict[attribute] < value:
                        output.append(i)
                elif operation == "!=":
                    if node_dict[attribute] != value:
                        output.append(i)
        return output

    def network_extract_attribute_values(self,attribute_list, attribute_type='node',start_node_list=[],network=None):
        """
        Extracts a set of given attribute values from a given set of start nodes.

        attribute_list:
        attribute_type: edge or node
        start_node_list: list of starting nodes

        """

        if network == None:
            network = self.network

        return_dict = {}
        if attribute_type == "node":
            for n in start_node_list:
                return_dict[n] = {}
                for nn in network[n]:
                    return_dict[n][nn] = {}

                    for a in attribute_list:
                        return_dict[n][nn][a] = network.node[n][a]

        elif attribute_type == "edge":
            for n in start_node_list:
                return_dict[n] = {}
                for nn in network[n]:
                    return_dict[n][nn] = {}
                    for a in attribute_list:
                        return_dict[n][nn][a] = network[n][nn][a]

        return return_dict


    def network_search_nodes(self, term_list, network=None,case_sensitive=False):
        """
        Searchfor a node(s) in the entire network with the given search criteria.
        Args:
         term_lsit(lits): list of sarch terms
         network(networx): network object
         exact(bool):search for eact word
         case_sensitive(bool): is the search case sensitive
        """
        if network == None:
            network = self.network
        results = []

        nodes = network.nodes()
        if case_sensitive == False:
            term_list_upper = [str(i).upper() for i in term_list]
            nodes_upper = [str(i).upper() for i in nodes]
        else:
            term_list_upper = term_list
            nodes_upper = nodes

        for i in zip(nodes,nodes_upper):
            for j in zip(term_list, term_list_upper):
                #not exact match
                if j[1] in i[1]:
                    results.append(i[0])
        return results


    def network_import_node_list(self,data_file,network=None):
        """
        Takes in a file where column 1 = nodeX column 2= node_list and joins nodeX with all nodes in column 2
        Args:
         data_file(str):file name and path
         network(networkx)
        """

        for line in open(data_file,'r'):
            temp = line.strip().split('\t')
            x = temp[0].strip()
            y = temp[1].strip().split(',')

        for i in y:
            g.add_edge(x,y)

        return g

    def network_import_file(self, data_file, network=None, list_format=True, has_attributes=True, nodeX_column=0, nodeY_column=1, attribute_columns=[],skip_rows=[],header_row=0,nodeX_prefix="", nodeY_prefix="",top_node=None):
        """
        [vetted]
        imports a datafile where the first two columns contain the two nodes to be joined and the rest contains attributes where the column header will have the attribute name.
        Args:
        data_file: tab delimited datafile with a header of one line
        network: custom network to be used
        list_format: is nodeY column a list?
        has_attributes: are there additional attribute columns
        nodeX: column number of the first node
        nodeY: column numbere of the second node
        attribute_column: list
        skip_rows: list of rows to skip
        header_row: the number of the header_row
        """

        if network == None:
            network = self.network

        c = 0
        header_list = []
        for line in open(data_file,'r'):

            temp = line.strip().split('\t')

            if attribute_columns == []:
                attribute_columns = range(len(temp))
                attribute_columns.remove(nodeX_column)
                attribute_columns.remove(nodeY_column)
                #print attribute_columns
            if c == header_row:
                #first line
                header_list = temp
                c = c + 1
                continue
            if top_node != None:
                network.add_edge(top_node, nodeX_prefix+temp[nodeX_column])
            if has_attributes == False:
                if list_format == False:
                    #one on one relationship
                    network.add_edge(nodeX_prefix+temp[nodeX_column],nodeY_prefix+temp[nodeY_column])
                else:
                    #one to many relationship
                    for j in temp[nodeY_column].strip().split(','):
                        network.add_edge(nodeX_prefix+temp[nodeX_column],nodeY_prefix+j)
            else:
                #there are attributes

                data_dict = {}
                for a in attribute_columns:
                    attribute_name = header_list[a]
                    attribute_value = temp[a]

                    data_dict[attribute_name] = attribute_value
                    #print data_dict


                if list_format == False:

                    network.add_edge(nodeX_prefix+temp[nodeX_column],nodeY_prefix+temp[nodeY_column],data_dict)
                else:
                    if "," not in line:
                        continue
                    for j in temp[nodeY_column].strip().split(','):

                        network.add_edge(nodeX_prefix+temp[nodeX_column],nodeY_prefix+j,data_dict)

                    """#one to many relationship
                    nodeX = temp[0].strip()
                    nodeY = temp[1].strip()
                    del temp[0]
                    del temp[1]
                    attr_list_of_lists = temp
                    data_dict = dict(zip(
                    c1 = -1 #counter
                    c2 = -1
                    for j in nodeY.strip().split(','):
                        c1 = c1 + 1
                        temp_dict = {} #attr_name:attr_value for the specific nodeY
                        for h in header_list:
                            c2 = c2+1
                            temp_dict[h] = attr_list_of_lists
                    """


	 def network_import_file_df(self, data_file, network=None,index_column=0, row_for_column=0):
        """
        g : networkx graph object
        data_file: the file which contains the data
        index_column: the column to create the index on
        column_row: row for the column names
        """
        if network == None:
            network = self.network
        df = pandas.read_csv(data_file, sep="\t")
        output = self.utils_dataframe_to_network(df,network, index_column, row_for_column)
        return output

    def network_link_with_term(self, term, node_name,network=None):
        """
        link all nodes with the search term into the given node
        """
        if network == None:
            network = self.network
        all_nodes = network.nodes()

        for n in all_nodes:
            if term in n:
                network.add_edge(node_name, n)
        return network


    def network_manual_link(self,nodeA, node_list,network=None):
        if network == None:
            network = self.network
        for n in node_list:
            network.add_edge(nodeA,n)
        return network

    def network_save_network(self,pickle_name):
        """
        saves the network as a pickle
        """
        import pickle
        pickle.dump(self.network, open(pickle_name,'wb'))



    def network_add_links_by_function(self, function_object, start_node=None,attribute_name=None,network=None):
        """
        creates links based on the results of a given function
        function_object(function): should take a single string as input
        start_node: the node to start the link
        attribute_name: name of the attribute to extract the value to be send to the function, if None will use node name
        network:
        start_node:

        """
        if network == None:
            network = self.network

        #get node list
        if start_node == None:
            #get all nodes
            nodes = network.nodes()
        else:
            nodes = network[start_node]

        for n in nodes:
            if attribute_name == None:
                #process node name
                output = function_object(n)
            else:
                #need to pass the attribute value
                nn = network.node[n][attribute_name]
                output = function_object(nn)
            try:
                network.add_edge(start_node,output)
    def network_function_to_links(self, function_object, start_node=None, network=None,node_attribute=None,add_top_node=True):
        """
        [V]
        Creates a link based on a function
        e.g. f(x) --> y[a] = v, then , v-->x and if add_top_node: a-->v

        Args:
        function_object: takes in a single value and returns a dict
        start_node: parent node of the node to be used in the link
        node_attribute: the attribute of the node to be used to select the input of the function, if None the node name is used
        add_top_node: link keys of the return dict with the value.
        """

        if network == None:
            network = self.network

        if start_node == None:
            nodes = network.nodes()
        else:
            nodes = network[start_node]

        for node in nodes:
            if node_attribute == None:
                output = function_object(node)
            else:
                input = network.node[node_attribute]
                output = function_object(input)

            for k in output.keys():
                if add_top_node:
                    network.add_edge(k,output[k])
                network.add_edge(output[k],node)
        return network

    def network_file_to_nodeY(self,file_name,nodeX,network=None):
        """
        [v]
        Takes in a file uses its likes to create nodeX --> nodeY


        """
        if network == None:
            network = self.network


        for line in open(file_name,"r"):
            network.add_edge(nodeX, line.strip())

        return network


    def network_overlap_nodes(self, node_list, network=None):

        if network == None:
            network = self.network

        overlap_set = set(network[node_list[0]])
        del node_list[0]
        for node in node_list:
            overlap_set = set(network[node]).intersection(overlap_set)

        return overlap_set


    def network_search(self, table, row_list="*", column_list = "*" ,index_column=""):

        """
        Extracts data from a given table with the given criteria

        table: name of the table
        row_list: list of index names as identified by index_columnm
        row_filter: a term to filter rows optional
        column_list: list of columns to select
        column_filter: optional filter for columns
        index_column: the column to define the index

        """
        import blaze,pandas
        df = blaze.Data('%s::%s' % (self.db_name,table))
        df = blaze.into(pandas.DataFrame,df)

        if row_list == "*":
            pass
        else:
            df.index = df[index_column]
            df = df.ix[row_list]
        if column_list == "*":
            pass
        else:
            df = df[column_list]
        return df


    def table_get_column_names(self, table, filter_list=[""]):
        """
        Returns all columns from a given table
        Args:
         table:
         filter_list(list): list of terms to filter from
        """
        df = blaze.Data('%s::%s' % (self.db_name,table))
        if filter_list == [""]:
            #no filter
            return df.columns
        output = []
        for i in df.columns:
            for j in filter_list:
                if j in i:
                    if i not in output:
                        output.append(i)
        return output


    def table_get_row_values(self,table,index_column,filter_list=[""]):
        """
        Returns the values of the given index_column
        Args:
         table:
         index_column: The column to be used for extracting values
         filter_list(list): list of terms to filter from
        """
        df = blaze.Data('%s::%s' % (self.db_name,table))
        output = []
        for i in df[index_column]:
            for j in filter_list:
                if j in i[0]:
                    if i[0] not in output:
                        output.append(i[0])
        return output
        #return list(blaze.into(pandas.Series,df[index_column]))

    def table_get_all_values(self,table):
        """
        Extracts all data from a given table
        Args:
         table:
        """
        df = blaze.Data('%s::%s' % (self.db_name,table))
        #ss = [df.columns[1]] + ss #adding the first column
        df2 = blaze.into(pandas.DataFrame,df)
        return  df2



    def util_list_filter_list(self,filter_list,main_list=None,case_sensitive=False):
        """
        Filters a given list of terms with a list of search terms.
        Args:
         main_list(list): the data list
         filter_list(list): list of search terms
         case_sensitive(bool): should the search be case sensitive
        """
        output = []
        if main_list == None:
            main_list = self.network.nodes()
        if case_sensitive == False:
            main_list_upper = [str(x).upper() for x in main_list]
            filter_list_upper = [str(x).upper() for x in filter_list]
        else:
            main_list_upper = main_list
            filter_list_upper = filter_list
        for i in zip(main_list,main_list_upper):
            for j in zip(filter_list, filter_list_upper):
                if j[1] in i[1]:
                    if i[0] not in output:
                        output.append(i[0])
        return output



def utils_file_to_network

"""
    def utils_dataframe_to_network(self,df,network=None, index_column=0, column_row=0, index_prefix="", value_prefix="", column_prefix=""):
        ""
        g : networkx graph object
        df: the dataframe which contains the data
        index_column: the column to create the index on
        column_row: row for the column names
        ""
        if network == None:
            network = self.network
        df.columns = df.ix[column_row]
        df.index = df[df.columns[index_column]]
        for i in range(1,len(df)-1):
            s = df.ix[i]
            columns = list(s.index)
            values = list(s.values)
            node_name = values[0]
            del(columns[0])
            del(values[0])
            for k,v in zip(columns,values):
                network.add_edge(index_prefix + str(node_name),value_prefix + str(v))
                network.add_edge(value_prefix + str(v),column_prefix + str(k))



        return network
"""


