# creates a graph of files based on the file structure

import subprocess
import sys
import networkx as nx
import cPickle
import os
import shelve


skip_nodes = ['gz', 'toProcess',"20151101"]
#test to see if pickle exists

network_pickle_loc = "/mnt/ceph_market/Shared/myCode/krakendb/krakenDB/utils/g.pickle"

if os.path.isfile(network_pickle_loc):
    #file exists
    g = cPickle.load(open(network_pickle_loc, "rb"))

else:
    g = nx.DiGraph()

folder = sys.argv[1].strip()

root_name = sys.argv[2].strip()

cmd = 'find %s -name "*.txt"' % folder
results= subprocess.check_output(cmd, shell=True).split()
for r in results:
    #print "working on r" , r

    data = r.strip().split('/')
    for i in xrange(len(data)-1):
        #print data[i], data[i+1]
        if data[i+1] in skip_nodes:
            data[i+1] = data[i]
            continue

        g.add_edge(data[i], data[i+1])

    if root_name != "*":
        #linking the given root_name to all the files
        g.add_edge(root_name, data[-1], path=r)

cPickle.dump(g, open(network_pickle_loc, "wb"), protocol=cPickle.HIGHEST_PROTOCOL)
