"""

Converts imformation in a metadata file into a network

"""

import py2neo
g = py2neo.Graph()

import sys
import pandas
file_name = sys.argv[1].strip()
df = pandas.read_csv(file_name)

#ASSUMPTIONS
"""
First column is the row header

Remove column if
 a. unique elements is 'almost' equal to the length of the dim
 b. there are ints and / or floats

"""


row_header_name = df.columns[0]
df.index = df[row_header_name]
del df[row_header_name]

file_node = py2neo.Node("file", name=file_name)
g.create(file_node)

row_node = py2neo.Node("file_rows", file=file_name)
g.create(row_node)

r1 = py2neo.Relationship(file_node,"row_of" , row_node)
g.create(r1)

for c in df.columns:
    factor_length = len(df[c].unique())
    print factor_length
    if factor_length == 1 or factor_length > 10:
        print "skipping ", c
        continue

    print "working on", c
    #create column node
    column_node = py2neo.Node("file_columns", name=c, file=file_name)
    g.create(column_node)


    #link file with file_columns

    r1 = py2neo.Relationship(file_node,"contains_column", column_node)
    g.create(r1)
    data = df[c].dropna()
    d = zip(data.index, data.values)
    tx = g.begin()


    for i in d:
        #print i
	value = i[1]
	ix = i[0]
        cnode = py2neo.Node("column_value", value=value) #factor
        tx.create(cnode)

        r1 = py2neo.Relationship(column_node, "factor_of", cnode)
        tx.create(r1)

        rnode = py2neo.Node("row_header", value=ix)
        tx.create(rnode)

        r1 = py2neo.Relationship(cnode, "has_value", rnode)
        tx.create(r1)

        r1 = py2neo.Relationship(row_node,"is_row", rnode)
        tx.create(r1)
    tx.commit()
    tx = g.begin()








