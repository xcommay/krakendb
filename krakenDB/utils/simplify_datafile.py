"""
Takes in a text file containing data and procsesses / splits it according to the following rules.

1. A single file should contain one type of information e.g. expression counts
2. Columns should be sample IDs
3. Rows should be gene IDs or similar which include genomic co-ordinates


"""

import sys
import pandas

data_file = sys.argv[1].strip()
comment_char = sys.argv[2].strip()
index_col = int(sys.argv[3].strip())
split_col = int(sys.argv[4].strip()) #the row that contains the value for splitting
sep = sys.argv[5].strip()
if sep == "tab":
    sep = "\t"

#df = pandas.read_csv(f, sep="\t", index_col=0, usecols=list(i))


f = open(data_file, "r")
pos = 0
while pos != split_col:
    _ = f.readline()
    pos = pos + 1

data = f.readline()
data = data.strip().split(sep)
#print data
index_col_name = data[index_col]
#print index_col_name
data = pandas.Series(data)

unique_values= data.unique()
for u in unique_values:
    #ignore the index_col name
    #print u, index_col_name
    if u == index_col_name:
        continue
    print "working on", u
    u_ix = list(data[data == u].index) + [index_col]
    #u_ix = list(data[data==u].index)
    #print u_ix
    df = pandas.read_csv(data_file, usecols=u_ix, sep=sep, header=0)
    #delete the row tha contains the split catigories i.e split_col
    df.drop(index_col, inplace=True)
    #df.drop(df.index[[1,3]], inplace=True)


    df.to_csv(data_file +"___" +  u +"___" + ".csv", index=False)



