import blaze
import pandas
import networkx
import cPickle

class krakenDB():

    def __init__(self,network_name=None):
        """
        Instantiate krakenDB class.
        Create a new network if a network is not given otherwise load it from the pickle file.

        Args
            network_name: name of network , default None
        """

        if network_name == None:
            #create a new network
            import networkx as nx
            self.network = nx.DiGraph()
        else:
	    #there is a network
            if ".pickle" in netwrok_name:
                #the network is pickled
                import cPickle
                network = cPickle.load(open(network_name), "rb")
                self.network = network



    def
