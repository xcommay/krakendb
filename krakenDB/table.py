import data
import blaze




def load_data_table(table_location, tech_domain, sample_domain, name, id_class):
    """
    Imports data_table information into the graph. Uses blaze to open any tabular format that it supports
    Each table should have sample names as column headers and gene names (or equvalent) as row headers


    Parameters
    __________
    table_location: str
    tech_domain: transcriptomics, proteomics, methylomics
    sample_domain: e.g. COAD
    name: name to be used in queries
    link_csv_file: link file joining the table rowIDs with ENREZIDs. None if the table has ENTREZ IDs
    """
    g = data.network
    g.add_edge("config_g_keys", id_class)
    df = blaze.data(table_location)
    columns = [i for i in df.fields]
    row_index = [i for i in df[columns[0]]]

    #adding information to network

    name = sample_domain + ":" + name
    g = data.network
    g.add_node("tech_domain")
    g.add_node("data_table")
    g.add_edge("tech_domain", sample_domain)
    g.add_edge(sample_domain, name)
    g.add_edge("data_table", name)


    g.add_node(name,domain=sample_domain, con_string=table_location, kind="data_file", columns = columns[1:], rows=row_index[1:], id_class=id_class)
    data.network = g


def search(rowID_list, colID_list, file_list):
    import blaze
    for f in file_list:
        df = blaze.data(f)
        index_column = df.fields[0]
        data = df[index_column].isin(rowID_list)

        return df[data][colID_list]



