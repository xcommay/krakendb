import data
import ast
from collections import Counter
import blaze


def create_empty(kind="networkx"):
    """
    Create a new network instance of a given kind

    Parameters
    ----------
    kind : str, default = 'networkx'
        The type of network to be used. Currently only supports in-memory networkx instances. Future will add presistance to networkX and neo4j.


    Returns
    -------

    data.network : The saved network object

    """
    if kind == 'networkx':
        import networkx as nx
        data.network = nx.DiGraph()
        data.network_kind = "networkx"
    return data.network


def load_from_pickle(file_path="network.pickle"):

    """
    Loads a network from a given pickle file

    """

    import cPickle
    data.network = cPickle.load(open(file_path,"rb"))
    data.network_kind = "networkx" #it is assumed that only networkx modules are pickled
    return data.network

def add_metadata_from_file(file_path, domain="UNKNOWN", row_ID_index = [0]):

    """
    imports data in a metadta file into the network.

    Notes
    -----
    Assumptions:
        first line in column header
        file is a csv
        file_name should contain the absolute path
        the metadata file should contain sample IDs as rowIDs and metadata columns as colomn IDs


    Parameters
    ----------

    file_path : str, absolute path of the metadata file
    row_ID_index : list, the index of rowIDs in file (a single file can have multiple rowID columns.
    domain: the domain of the file, (in the case of tcga data it could be the name of the cancer
    """

    import pandas
    df = pandas.read_csv(file_path)
    file_name = file_path.split('/')[-1].split('.')[0]

    g = data.network
    if g == None:
        #there is no network
        import networkx as nx
        g = nx.DiGraph()
    g.add_edge("metadata_source", "metadata_file")
    g.add_edge("metadata_file", file_name, location=file_path, kind='metadata_file', domain=domain)
    import ast
    for r in row_ID_index:
        df.index = df[df.columns[r]]
        del df[df.columns[r]]

        for c in df.columns:
            g.add_node(c, kind="meta_property")
            g.add_edge(file_name,c)
            fdata = df[c].dropna()
            ix = fdata.index
            ix_values = fdata.values
            length = len(ix)
            #g.add_node(c, kind="meta_property")
            g.add_edge(file_name, c, kind="file_to_meta_property")
            if df[c].dtype == float or df[c].dtype == int:
                #the column contains numerical values
                #format column ---- rowid with value as an edge property
                for i,v in zip(ix, ix_values):
                    g.add_node(i, kind="sample")
                    g.add_edge(c,i, value=v, domain=domain)
            else:
                #the column contains text
                g.add_nodes_from(ix, kind='sample')
                g.add_nodes_from(ix_values, kind='meta_property_catagorical_value')
                g.add_edges_from(zip([c] * length,ix_values))
                g.add_edges_from(zip(ix_values, ix), domain=domain)
        #g.add_edges_from(zip([file_name]* length , ix))

    data.network = g
    return data.network



def import_node_list(data_file,id_class,right_prefix="ENTREZ:"):
    """
    Takes in a file where column 1 = nodeX column 2= node_list and joins nodeX with all nodes in column 2
    Args:
     data_file(str):file name and path
     network(networkx)
    """
    g = data.network
    for line in open(data_file,'r'):
        temp = line.strip().split(',')
        x = temp[0].strip()
        y = temp[1].strip().split(',')
        for i in y:
            g.add_edge(id_class, x)
            i = right_prefix + i
            g.add/_edge(i,x)

    return g




def add_metadata_from_folder(folder_name,domain="UNKNOWN"):
    """
    Goes through all csv files in a given filder and adds their data into the network

    Notes
    -----

    Meta data files should be csvs and have only one rowID column in index 0.

    Parameters
    ----------
    folder_name : str, name of the folder that contains the metadata files




    """

    import glob
    files = glob.glob(folder_name + "/*.csv")

    for f in files:
        print f
        add_metadata_from_file(f, domain=domain)


def pickle_network(pickle_name="network.pickle"):
    """
    Saves the network as a pickle file

    """

    import cPickle
    cPickle.dump(data.network, open(pickle_name, "wb"), protocol=cPickle.HIGHEST_PROTOCOL)


def oQuery(g,s,t):
    """
    main query function for queries.

    Parameters
    -----------
    g : str, string of commands which describe the genes to be used
    s:str, string of commands to extract samples
    t: str, string with the omics files ti be used in the search

    """
    sample = sample_search(s)
    gene = gene_search(g, sample)
    tech = tech_search(genes, samples, t)



def tech_search(genes, samples, tech):
    import feather
    g  = data.network
    for t in tech.strip().split("+"):
        t = t.replace('.', ':')
        idc = g.node[t]['id_class']
        con_string = g.node[t]['con_string']
        d = blaze.data(con_string)
        import pandas
        df = odo.odo(d, pandas.DataFrame)
        df.index = df[df.column[0]]
        del df.column[0]

        common_samples = set(df.columns).intersection(samples)

        if idc == 'ENTREZ':
            e = [i.split(':')[1] for i in list(genes)]
            feather.writedataframe(df.ix[e], t + ".feather")
        else:
            e = []
            for gene in genes:
                a = set(g[gene].keys())
                b = set(g[idc])
                e = e + list(a.intersection(b))
            feather.writedataframe(df.ix[e], t + ".feather")


def gene_search(command,samples):
    """
    provides a list of ENTREZ gene id  using the given search string. The function requires a valid set if samples generated by sampke_search function.

    """

    g_keys=["PATHWAY", "ENTREZ", "map_location", "type_of_gene"]
    g = data.network
    g_keys = g['config_g_keys']
    comp_operators = ["==", ">=", "<=", ">", "<"]

    symbols = "+-&|"
    command_order = []
    symbol_order = []


    for c in command:
        if c in symbols:
            symbol_order.append(c)

    print "so", symbol_order



    for s in symbols:
        command = command.replace(s, "_@@_")
    print command
    command_order = command.strip().split("_@@_")
    print command_order


    final_output = set()
    counter = 0
    for cmd in command_order:
        cmd = cmd.strip()
        output_genes = [] #temp container for storing genes in this iteration
        if "." not in cmd and ":" not in cmd:
            #simple gene id
            cmd = "SYMBOL:" + cmd.strip()

        if ":" in cmd:
            first,second = cmd.strip().split(":")
            if first == "ENTREZ":
                #entrez gene id
                output_genes.append(cmd.strip())

            elif first in g_keys: #e.g. pathway
                output_genes = g[second]

            else:#gene synonyms e.g ENSEMBL
                output_genes = [i for i in g.predecessors(cmd) if "ENTREZ" in i]

        if "." in cmd:#need to access data file e.g COAD.methylation > 0.5
            concat_output = []
            #a numeric comparison of a data table
            gene_list_pass = []
            for c in comp_operators:
                if c in cmd:
                    name,number = cmd.strip().split(c)
                    first,second = name.strip().split('.')
                    n = first  + ":" + second
                    con_string = g.node[n]['con_string']
                    df = blaze.data(con_string)
                    index  = df.fields[0]
                    common_columns = list(set(samples).intersection(set(df.fields))) + [index]
                    df = df[common_columns] #uses only the filtered samples [TODO]
                    fields = df.fields
                    gene_list = []
                    for f in fields:#iterate through all the fields / samples and find the genes that match the criteria. Then get the intersection.
                        if f == index:
                            continue

                        #df[df['TCGA-G4-6311-01A-11R-1723-07']>= 0.5][index]


                        import odo, pandas
                        dff= odo.odo(df, pandas.DataFrame)
                        s = "dff[dff['" + f + "']"  + c + number + "]['" + index+"']"
                        print s
                        data2 = eval(s)
                        #gene_list = set()
                        first_loop = False
                        for d in data2:
                            #print "gs", gene_list
                            #add the index / rowID as intersection to gene_list
                            if g.node[n]['id_class'] == 'ENTREZ':
                                gene_list.append("ENTREZ:" + str(d)) #take this out of loop for speed [TODO]
                            else:#other ID
                                if len([i for i in g.predecessors(d) if "ENTREZ" in i]) >= 1:

                                    gene_list = gene_list + [i for i in g.predecessors(d) if "ENTREZ" in i]

                    counts = Counter(gene_list)
                    for count in counts.keys():
                        if counts[count] >= len(common_columns)-1:
                            gene_list_pass.append(count)


            output_genes = gene_list_pass
        #aggregation
        if counter == 0:
            final_output = set(output_genes)
        else:
            print counter, symbol_order
            symbol= symbol_order[counter-1].strip()
            if symbol == "+" or symbol == "|":
                #union
            #if have_samples == False:
                final_output = final_output.union(output_genes)
            elif symbol == "&":
                final_output = final_output.intersection(output_genes)
            elif symbol =="-":
                final_output = final_output - output_genes
        counter = counter + 1
    return  final_output




                    #iterate all columns one by one and create a (gene,sample) list
    return output_genes




def sample_search(command):

    final_output = set()
    counter = 0
    for cmd in command.strip().split("->"):
        output = simple_search(cmd.strip())

        if counter == 0:
            #first iteration
            final_output = output

        else:
            final_output = final_output.intersection(output)
        counter = counter + 1

    return final_output

def simple_search(command):

    #i_identifiers = [
    symbols = "+-&|" #allowed symbols
    #command =  "COAD:TP + COAD:bmi < 0.003"


    comp_operators = ["==", ">=", "<=", ">", "<"]

    #get symbol order
    symbol_order = []
    for c in command:
        #print c
        if c in symbols:
            symbol_order.append(c)



    #get command order
    for s in symbols:
        command = command.replace(s, "_@@_")

    command_order = command.strip().split("_@@_")

    print command_order, symbol_order

    final_output = set()
    counter = 0
    for c in command_order:
        #see if there is a query for a numeric value
        numeric_comparison = False
        comp_operators = ["==", ">=", "<=", ">", "<"]
        for comp in comp_operators:
            if comp in c:
                numeric_comparison = comp
                break

        if numeric_comparison !=  False:
            #do a numeric comparison
            left, right = c.strip().split(numeric_comparison)
            right = ast.literal_eval(right.strip())
            domain, node  = left.strip().split(':')
            output = one_step_multi_filter_search(node, edge_filters=[("domain","==",domain),("value", numeric_comparison, right)])

        else:#COAD.TP
            #just a catagorical traversal
            domain, node = c.strip().split(":")
            output = one_step_multi_filter_search(node, edge_filters=[("domain", "==", domain)])

        if counter == 0:
            final_output = output
        else:
            symbol= symbol_order[counter-1].strip()
            if symbol == "+" or symbol == "|":
                #union
                final_output = final_output.union(output)
            elif symbol == "&":
                final_output = final_output.intersection(output)
            elif symbol =="-":
                final_output = final_output - output
        counter = counter + 1
    return  final_output



def one_step_multi_filter_search( start_node,node_filters=(), edge_filters=()):
    """
    Does a one step traversal based on the given node_filter and edge_filter.

    Args:
     start_node: name of the start node
     node_filter: tuple (attribute,operation,value)
     edge_filter: tuple (attribute,operation,value)
     attribute: name of attribute
     operation: ==, <, <=, >,>=, in
    """
    print start_node, node_filters, edge_filters
    output = []
    network = data.network
    node_dict = network[start_node]
    if edge_filters == () and node_filters == ():
        #a traversing without any filter
        return node_dict.keys()

    for i in node_dict:
        edge_filter_results = []
        for edge_filter in edge_filters:
            attribute,operation,value = edge_filter

            if operation == "in":
                if value in node_dict[i][attribute]:
                    output.append(i)
            elif operation == "not in":
                if value not in node_dict[i][attribute]:
                    output.append(i)
            elif operation == "==":
                if node_dict[i][attribute] == value:
                    output.append(i)
            elif operation == ">=":
                if node_dict[i][attribute] >= value:
                    output.append(i)
            elif operation == "<=":
                if node_dict[i][attribute] <= value:
                    output.append(i)
            elif operation == ">":
                if node_dict[i][attribute] > value:
                    output.append(i)
            elif operation == "<":
                if node_dict[i][attribute] < value:
                    output.append(i)
            elif operation == "!=":
                if node_dict[i][attribute] != value:
                    output.append(i)

            #edge_filter_results.append(output)
            #output = []
        #to pass all the edge_filters the id should occur n times where n = length of edge filter
        output_pass = []
        counts = Counter(output)
        for count in counts.keys():
            if counts[count] >= len(edge_filters):
                output_pass.append(count)

        output2 = []
        for j in output_pass:
            for node_filter in node_filters:
                attribute,operation,value = node_filter
                node_dict = network.node[j]
                if operation == "in":
                    if value in node_dict[attribute]:
                        output2.append(j)
                elif operation == "not in":
                    if value not in node_dict[attribute]:
                        output2.append(j)
                elif operation == "==":
                    if node_dict[attribute] == value:
                        output2.append(j)
                elif operation == ">=":
                    if node_dict[attribute] >= value:
                        output2.append(j)
                elif operation == "<=":
                    if node_dict[attribute] <= value:
                        output2.append(j)
                elif operation == ">":
                    if node_dict[attribute] > value:
                        output2.append(j)
                elif operation == "<":
                    if node_dict[attribute] < value:
                        output2.append(j)
                elif operation == "!=":
                    if node_dict[attribute] != value:
                        output2.append(j)

        output2_pass = []
        counts = Counter(output2)
        for count in counts.keys():
            if counts[count] >= len(node_filters):
                output2_pass.append(count)

    if output2_pass == []:
        return set(output_pass)
    else:
        return set(output2_pass)



def oSearch(g,s,t):
    """
    Carry out an omics search

    order : g-> s-> t




    """

    s_output = complex_search(s)

